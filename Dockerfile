#version 0.0.1
FROM python:3.6
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/
COPY . .
RUN python -m pip install fastapi[all]

RUN python -m pip install uvicorn

CMD ["python", "main.py"]
